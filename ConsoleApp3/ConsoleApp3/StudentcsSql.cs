﻿using ConsoleApp3.Data;

namespace ConsoleApp3
{
    public class StudentcsSql
    {
        public static List<Data.Student> InquireData(string name)
        {
            var itme =  Builde.FSql
                             .Select<Student>()
                             .Where(w => w.Name == name)
                             .ToList();
            return itme;
        }
        public static int AddData(string? name, int? age, string? calss)
        {
            var itme = new Data.Student()
            {
                Name = name,
                Age = age,
                Calss = calss
            };
            var count = Builde.FSql
                              .Insert(itme)
                              .ExecuteAffrows();
            return count;
        }
        public static int DeleteData()
        {
            var count = Builde.FSql
                               .Delete<Student>()
                               .Where(w => w.Name == "张三")
                               .ExecuteAffrows();
            return count;
        }
    }
}
