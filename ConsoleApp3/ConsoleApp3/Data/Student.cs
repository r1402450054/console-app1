﻿using FreeSql.DataAnnotations;

namespace ConsoleApp3.Data
{
    public class Student
    {
        [Column(IsIdentity = true, IsPrimary = true)]
        public int? ID { get; set; }
        public string? Name { get; set; } = "";
        public int? Age { get; set; } = 0;
        public string? Calss { get; set; } = "";

    }
}
