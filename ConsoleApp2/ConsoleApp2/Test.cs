﻿using ConsoleApp2.Model;

namespace ConsoleApp2
{
    public class Test
    {

        public static List<Model.UserInfo> GetItems(string name)
        {
            var items = Builder.FSql
                               .Select<Model.UserInfo>()
                               .Where(w => w.Name == name)
                               .ToList();
            return items;
        }

        public static List<Model.UserInfo> GetItems()
        {
            var items = Builder.FSql
                               .Select<Model.UserInfo>()
                               .ToList();
            return items;
        }


        public static Model.UserInfo GetItem(string name)
        {
            var item = Builder.FSql
                              .Select<Model.UserInfo>()
                              .Where(w => w.Name == name)
                              .ToOne();
            return item;
        }

        public static async Task<Model.UserInfo> GetItemAsync(string name)
        {
            var item = await Builder.FSql
                                    .Select<Model.UserInfo>()
                                    .Where(w => w.Name == name)
                                    .ToOneAsync();
            return await Task.FromResult(item);

        }

        public static int AddListData()
        {
            var items = new List<Model.UserInfo>
            {
                 new Model.UserInfo() {  Name = "Test2",  Email = "Test2@qq.com", },
                 new Model.UserInfo() {  Name = "Test3",  Email = "Test3@qq.com", },
                 new Model.UserInfo() {  Name = "Test4",  Email = "Test4@qq.com", },
            };

            var count = Builder.FSql
                               .Insert(items)
                               .ExecuteAffrows();
            return count;
        }

        public static int AddData()
        {
            var item = new Model.UserInfo()
            {
                Name = "Test",
                Email = "Test@qq.com",
            };

            var count = Builder.FSql
                               .Insert(item)
                               .ExecuteAffrows();
            return count;
        }

        public static int DeleteData(string name)
        {
            var count = Builder.FSql
                               .Delete<UserInfo>()
                               .Where(w => w.Name == name)
                               .ExecuteAffrows();
            return count;
        }

    }
}
