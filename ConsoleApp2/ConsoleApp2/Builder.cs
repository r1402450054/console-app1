﻿namespace ConsoleApp2
{
    public class Builder
    {
        static string connectionString = "Data Source=.;User Id=sa;Password=sa;Initial Catalog=Test;Encrypt=True;TrustServerCertificate=True;Pooling=true;Min Pool Size=1";

        public static IFreeSql FSql = new FreeSql.FreeSqlBuilder()
                                                 .UseConnectionString(FreeSql.DataType.SqlServer, connectionString)
                                                 .UseAutoSyncStructure(true) //自动同步实体结构到数据库
                                                 .Build(); //请务必定义成 Singleton 单例模式

    }
}
