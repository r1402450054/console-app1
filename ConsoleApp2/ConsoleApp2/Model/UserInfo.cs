﻿using FreeSql.DataAnnotations;

namespace ConsoleApp2.Model
{
    public class UserInfo
    {

        [Column(IsIdentity = true, IsPrimary = true)]
        public int ID { get; set; } = 0;

        public string Name { get; set; } = "";

        public string Email { get; set; } = "";
    }
}
