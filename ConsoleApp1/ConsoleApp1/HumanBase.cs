﻿namespace Practice
{/// <summary>
/// 人
/// </summary>
    public class HumanBase
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int? Age { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public double? Height { get; set; }
        /// <summary>
        /// 体重
        /// </summary>
        public double? Weight { get; set; }

        /// <summary>
        /// 说
        /// </summary>
        /// <returns>自我介绍</returns>
        public virtual string Talk()
        {
            var HumanBase = new HumanBase();
            HumanBase.Name = "张三";
            HumanBase.Age = 20;
            HumanBase.Height = 1.8;
            HumanBase.Weight = 80;
            string talk = $"我叫{HumanBase.Name}，是男人,今年{HumanBase.Age}岁,身高{HumanBase.Height}米,体重{HumanBase.Weight}kg"; ;
            return talk;
        }

    }
}
