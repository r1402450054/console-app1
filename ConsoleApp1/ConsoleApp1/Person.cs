﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    public class Person

    {   /// <summary>
        /// Person构造函数
        /// </summary>
        /// <param name="name">姓名</param>
        /// <param name="age">年龄</param>
        /// <param name="address">地址</param>
        public Person(string name, int age, string address)
        {
            Name = name; Age = age; Address = address;
        }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 打印
        /// </summary>
        /// <param name="person">对象张三</param>

        public static void Speak(Person zhangSan)
        {
            Console.WriteLine($"名字：{zhangSan.Name}\t年龄：{zhangSan.Age}岁\t地址：{zhangSan.Address}");
        }


    }

}
