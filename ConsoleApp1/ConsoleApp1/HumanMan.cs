﻿namespace Practice
{
    public class HumanMan : HumanBase
    {
        public HumanMan()
        {
        }

        public override string Talk()
        {
            var humanMan = new HumanMan();
            Console.WriteLine("请输入姓名");
            humanMan.Name = (Console.ReadLine());
            Console.WriteLine("请输入年龄");
            humanMan.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入身高(cm)");
            humanMan.Height = double.Parse(Console.ReadLine());
            Console.WriteLine("请输入体重(kg)");
            humanMan.Weight = double.Parse(Console.ReadLine());
            string talk = $"我叫{humanMan.Name}，是男人,今年{humanMan.Age}岁,身高{humanMan.Height}cm,体重{humanMan.Weight}kg";
            return talk;
        }


    }
}
