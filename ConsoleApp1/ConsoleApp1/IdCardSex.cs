﻿namespace Practice
{

    /// <summary>
    /// 2023年10月20日练习 身份证获取性别
    /// </summary>
    internal class IdCard
    {

        /// <summary>
        /// 枚举
        /// </summary>
        public enum SexEnum
        {
            /// <summary>
            /// 男性
            /// </summary>
            Male = 0,
            /// <summary>
            /// 女性
            /// </summary>
            Female = 1,
        }

        /// <summary>
        /// 获取性别
        /// </summary>
        /// <param name="id">身份证号码</param>
        /// <returns>性别</returns>
        public static SexEnum GetSex(string id)
        {
            string str = id.Substring(16, 1);
            int intSex = int.Parse(str);

            if (intSex % 2 != 0)
            {
                return SexEnum.Male;
            }
            else
            {
                return SexEnum.Female;
            }
        }

        /// <summary>
        /// 获取性别
        /// </summary>
        /// <param name="sex">枚举值</param>
        /// <returns></returns>
        public static string GetSex(SexEnum sex)
        {
            if (sex == SexEnum.Male)
            {
                return "性别是：男性";
            }
            else
            {
                return "性别是：女性";
            }
        }

        /// <summary>
        /// 获取生日日期
        /// </summary>
        /// <param name="id">身份证号码</param>
        /// <returns>生日日期</returns>
        public static DateTime GetBirthday(string id)
        {
            string y = id.Substring(6, 4);
            string m = id.Substring(10, 2);
            string d = id.Substring(12, 2);

            int year = int.Parse(y);
            int month = int.Parse(m);
            int day = int.Parse(d);

            var date = new DateTime(year, month, day);
            return date;
        }

    }
}
