﻿namespace Practice
{
    public class Rectangle
    {
        public int Length;
        public int Width;

        public Rectangle()
        {
        }

        //~Rectangle()
        //{

        //}

        /// <summary>
        /// 矩形
        /// </summary>
        /// <param name="length">长</param>
        /// <param name="width">宽</param>
        public Rectangle(int length, int width)
        {
            Length = length;
            Width = width;
        }

        /// <summary>
        /// 面积
        /// </summary>
        public double GetArea()
        {
            return GetArea(Length, Width);
        }

        public double GetArea(int length, int width)
        {
            int area = length * width;
            return area;
        }
        /// <summary>
        /// 周长
        /// </summary>
        /// <returns></returns>
        public double GetPerimeter()
        {
            return GetPerimeter(Length, Width);
        }

        public double GetPerimeter(int length, int width)
        {
            int perimeter = (length + width) * 2;
            return perimeter;
        }
       
    }
}
