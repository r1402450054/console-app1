﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Practice.Equation;

namespace Practice
{/// <summary>
/// 一元二次方程
/// </summary>
    public class Equation
    {   
        /// <summary>
        /// 根
        /// </summary>
        public struct Squareroot
        {
            public double X1;
            public double X2;
            public string X3;
        }

        /// <summary>
        /// 解方程
        /// </summary>
        /// <param name="a">已知数</param>
        /// <param name="b">已知数</param>
        /// <param name="c">已知数</param>
        /// <returns></returns>
        public Squareroot Unbind(double a, double b, double c)
        {
            Squareroot Squareroot = new();
            var v1= b * b - 4 * a * c;
            if (v1>= 0)
            {
                Squareroot.X1 = (-b + Math.Sqrt(v1)) / (2 * a);
                Squareroot.X2 = (-b - Math.Sqrt(v1)) / (2 * a);
            }
            else
            {
                Squareroot.X3 = "无解";
            }
            return Squareroot;
        }
    }
}
