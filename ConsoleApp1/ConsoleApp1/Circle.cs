﻿namespace Practice
{/// <summary>
 /// 圆
 /// </summary>
    public class Circle
    {
        public double Radius;
        public const double PI = 3.14;
        public Circle()
        {
        }
        /// <summary>
        /// 圆
        /// </summary>
        /// <param name="radius">半径</param>
        public Circle(double radius)
        {
            Radius = radius;
        }
        /// <summary>
        /// 面积面积保留两位小数
        /// </summary>
        /// <returns>面积保留两位小数</returns>
        public double GetArea()
        {
            double area = GetArea(Radius);
            double rounarea =Math.Round(area, 2);
            return rounarea;
        }
        /// <summary>
        /// 面积
        /// </summary>
        /// <param name="radius">半径</param>
        /// <returns>面积</returns>
        public double GetArea(double radius)
        {
            double area = PI * radius * radius;
            return area;
        }
        /// <summary>
        /// 周长保留两位小数
        /// </summary>
        /// <returns>周长保留两位小数</returns>
        public double  GetPerimeter()
        {
            double perimeter = GetPerimeter(Radius);
            double rounperimeter = Math.Round(perimeter, 2);
            return rounperimeter;
        }
        /// <summary>
        /// 周长
        /// </summary>
        /// <param name="radius">半径</param>
        /// <returns></returns>
        public double GetPerimeter(double radius)
        {
            double perimeter = 2 * PI * radius;
            return perimeter;
        }
    }
}

