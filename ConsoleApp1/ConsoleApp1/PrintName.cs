﻿namespace PrintName
{//练习
    public class Name
    {
        public string? Name1 { get; set; }
    }
    public static class NamePrint
    {
        public static void NamePrint1()
        {
            Name name2 = new() { Name1 = "张三" };
            Console.WriteLine(name2.Name1);
        }

    }
    public class NamePrint2
    {
        public void NamePrint3()
        {
            Name name3 = new() { Name1 = "李四" };
            Console.WriteLine(name3.Name1);
        }

    }
}
