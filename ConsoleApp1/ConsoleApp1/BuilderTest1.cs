﻿using System.Text;

namespace Practice
{
    public class BuilderTest1
    {
        public static string Test()
        {
            int num = 1000;
            var sbTest = new StringBuilder("荣耀自称科技标杆", 100);
            sbTest.Append($"VS小米死磕性价比{num:C}");
            sbTest.Insert(0,"PK:");
            sbTest.Remove(20, sbTest.Length - 20);
            sbTest.Replace("PK", "相爱相杀");
            string temp = sbTest.ToString();
            return temp;
        }
    }
}
