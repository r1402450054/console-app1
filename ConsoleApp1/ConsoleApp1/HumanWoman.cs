﻿namespace Practice
{
    internal class HumanWoman : HumanBase
    {
        public HumanWoman()
        {
        }

        public override string Talk()
        {
            var humanWoman = new HumanWoman();
            Console.WriteLine("请输入姓名");
            humanWoman.Name = (Console.ReadLine());
            Console.WriteLine("请输入年龄");
            humanWoman.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入身高(cm)");
            humanWoman.Height = double.Parse(Console.ReadLine());
            Console.WriteLine("请输入体重(kg)");
            humanWoman.Weight = double.Parse(Console.ReadLine());
            string talk = $"我叫{humanWoman.Name}，是女人,今年{humanWoman.Age}岁,身高{humanWoman.Height}cm,体重{humanWoman.Weight}kg";
            return talk;
        }
    }
}
