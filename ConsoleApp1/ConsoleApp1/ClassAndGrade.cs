﻿namespace Practice
{/// <summary>
/// 班级
/// </summary>
    public class ClassAndGrade
    {/// <summary>
     /// 成绩表
     /// </summary>
     /// <param name="count">学生序号</param>
     /// <returns></returns>
        public static int[,] GradeTable(int count)
        {
            int[,] gameTable = new int[count, 4];
            for (int i = 0; i < count; i++)
            {
                Console.Write($"请输入第{i + 1}学生编号");
                gameTable[i, 0] = Convert.ToInt32(Console.ReadLine());
                Console.Write("请输入语文成绩");
                gameTable[i, 1] = Convert.ToInt32(Console.ReadLine());
                Console.Write("请输入数学成绩");
                gameTable[i, 2] = Convert.ToInt32(Console.ReadLine());
                Console.Write("请输入英语成绩");
                gameTable[i, 3] = Convert.ToInt32(Console.ReadLine());
            }

            return gameTable;
        }

    }
}

