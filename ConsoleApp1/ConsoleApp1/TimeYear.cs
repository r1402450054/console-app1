﻿namespace Time
{
    internal class Year
    {
        //2023年/10/18日 润年判断函数
        public static bool RunYear(int year)
        {

            if (year % 4 == 0)
            {
                if (year % 400 == 0)
                {
                    return true;
                }
                else
                {
                    if (year % 100 != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else { return false; }

        }
    }
}
