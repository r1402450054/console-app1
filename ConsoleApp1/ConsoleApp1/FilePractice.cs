﻿using System.IO;
using System.Text;

namespace Practice
{
    public class FilePractice
    {

        /// <summary>
        /// 存储名单
        /// </summary>
        public static void StorageList()
        {

            string list1 = @"C:\Users\ye\Desktop\List1";
            string list2 = @"C:\Users\ye\Desktop\List2";
            string list3 = @"C:\Users\ye\Desktop\List3";
            string list4 = @"C:\Users\ye\Desktop\List4";
            string zhangSan1 = @"C:\Users\ye\Desktop\List1\ZhangSan.txt";
            string zhangSan2 = @"C:\Users\ye\Desktop\List2\ZhangSan.txt";


            // if (!Directory.Exists(list1))
            // {
            //     Directory.CreateDirectory(list1);
            //     Console.WriteLine($"{list1}创建成功");
            // }
            // if (!File.Exists(zhangSan1))
            // {
            //     using (File.Create(zhangSan1))
            //     {
            //     }
            // }
            // HumanMan humanMan = new();
            // string str = humanMan.Talk();
            // using (FileStream fs = File.OpenWrite(zhangSan1))
            // {
            //     byte[] info =
            //new UTF8Encoding(true).GetBytes(str);


            //     fs.Write(info, 0, info.Length);
            // }
            // if (!Directory.Exists(list2))
            // {
            //     Directory.CreateDirectory(list2);
            //     Console.WriteLine($"{list2}创建成功");
            // }
            // if (File.Exists(zhangSan1))
            // {
            //     File.Move(zhangSan1, zhangSan2);
            //     Directory.Delete(list1);
            // }
            // DirectoryInfo directoryInfo = new DirectoryInfo(list2);
            // FileInfo[] fileInfos = directoryInfo.GetFiles();
            // for (int i = 0; i < fileInfos.Length; i++)
            // {
            //     Console.WriteLine(fileInfos[i]);
            // }
            File.Delete(zhangSan2);
            if (!Directory.Exists(list3))
            {
                Directory.CreateDirectory(list3);
            }
            DirectoryInfo info4 = new(list4);
            info4.Create();
            Directory.Move(list3, @"C:\Users\ye\Desktop\List4\list3");
            Directory.Delete(@"C:\Users\ye\Desktop\List4\list3");
            info4.MoveTo(@"C:\Users\ye\Desktop\List2\list4");
            DirectoryInfo info2 = new(list2);
           
            var list = info2.GetDirectories();
            foreach (var b in list)
            {
                if (b is DirectoryInfo)
                {
                    Console.WriteLine(b);
                }
            }
            if (Directory.Exists(@"C:\Users\ye\Desktop2\list4"))
            {
                Directory.Delete(@"C:\Users\ye\Desktop2\list4");
            }
            info2.Delete(true);
            info2.Create();
        }
    }

}
